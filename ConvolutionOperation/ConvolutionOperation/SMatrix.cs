﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvolutionOperation
{
    class SMatrix
    {
        public int[,] Array { get; set; }

        public SMatrix(int[,] array)
        {
            Array = array;
        }

        public SMatrix() { }

        public static SMatrix operator *(SMatrix a, SMatrix b)
        {
            int[,] solution = new int[a.Array.GetLength(0) - b.Array.GetLength(0) +1, a.Array.GetLength(1) - b.Array.GetLength(1) + 1];

            for (int x = 0; x < solution.GetLength(0); x++)
            {
                for (int y = 0; y < solution.GetLength(1); y++)
                {
                    for (int i = 0; i < b.Array.GetLength(0); i++)
                    {
                        for (int j = 0; j < b.Array.GetLength(1); j++)
                        {
                            solution[x, y] += a.Array[x + i, y + j] * b.Array[i, j];
                        }
                    }
                }
            }

            SMatrix solutionmatrix = new SMatrix();
            solutionmatrix.Array = solution;

            return solutionmatrix;
        }
    }
}
