﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvolutionOperation
{
    class Program
    {
        static void Main(string[] args)
        {
            SMatrix a = new SMatrix();
            SMatrix b = new SMatrix();

            int[,] arr = new int[3,5];
            int[,] arr2 = new int[2, 2];

            Random rand = new Random();

            Console.WriteLine("Matrix 1: ");
            for (int x = 0; x < arr.GetLength(0); x++)
            {
                for (int y = 0; y < arr.GetLength(1); y++)
                {
                    arr[x, y] = rand.Next(1, 5);
                    Console.Write(arr[x, y] + "|");
                }

                Console.WriteLine();
            }

            Console.WriteLine("\nMatrix 2: ");
            for (int x = 0; x < arr2.GetLength(1); x++)
            {
                for (int y = 0; y < arr2.GetLength(0); y++)
                {
                    arr2[x, y] = rand.Next(1, 5);
                    Console.Write(arr2[x, y] + "|");
                }

                Console.WriteLine();
            }


            Console.WriteLine("\nLösungsmatrix: ");
            a.Array = arr;
            b.Array = arr2;

            SMatrix solution = a * b;

            for (int x = 0; x < solution.Array.GetLength(0); x++)
            {
                for (int y = 0; y < solution.Array.GetLength(1); y++)
                {
                    Console.Write(solution.Array[x, y] + "|");
                }
                Console.WriteLine();
            }

            Console.ReadLine();
        }
    }
}
