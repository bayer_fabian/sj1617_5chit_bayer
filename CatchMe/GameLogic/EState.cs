﻿

namespace GameLogic
{
    public enum EState
    {
        STOPPED = 0, RUNNING = 1, LOST = 2, WON = 3
    }
}
