﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CatchMe
{
    class Apple : IShape
    {
        int widht;
        int height;

        int x1;
        int X1
        {
            get { return x1; }
            set { x1 = value; }
        }

        int y1;
        int Y1
        {
            get { return y1; }
            set { y1 = value; }
        }

        int r;
        int R
        {
            get { return r; }
            set { r = value; }
        }

        private Pen p;

        public Pen P
        {
            get { return p; }
            set { p = value; }
        }

        public Apple(Pen p, int x1, int y1, int r)
        {
            P = p;
            X1 = x1;
            Y1 = y1;
            R = r;
            widht = 2 * r;
            height = 2 * r;
        }

        public void DrawMe(Graphics graphics)
        {
            graphics.DrawEllipse(P, new Rectangle(X1 - widht / 2, Y1 - height / 2, widht, height));
        }
    }
}
