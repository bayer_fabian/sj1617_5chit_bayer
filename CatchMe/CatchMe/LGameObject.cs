﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchMe
{
    class LGameObject
    {
        public LogicLine Line { get; set; }
        public int Height { get; set; }

        public LGameObject()
        {

        }

        public LGameObject(Line line, int height)
        {
            Line = line;
            Height = height;
        }
    }
}
