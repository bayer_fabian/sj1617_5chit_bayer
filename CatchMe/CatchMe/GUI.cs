﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
namespace CatchMe
{
    public partial class GUI : Form
    {
        List<Line> drawableLines;
        Apple basket;
        Apple apple;
        LGame game;
        EState gameState;


        public GUI()
        {
            InitializeComponent();

            DoubleBuffered = true;
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            game = new LGame(7, 5);
            gameState = game.GameStart();
            if (gameState == EState.RUNNING)
            {
                DrawLines();
                timer1.Enabled = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (gameState == EState.RUNNING)
            {
                gameState = game.MoveApple();
                panel1.Invalidate();
                lblScore.Text = game.Points.ToString();

                if (gameState == EState.LOST)
                {
                    MessageBox.Show("GAME OVER");
                }
            }
        }

        private void DrawLines()
        {
            drawableLines = new List<Line>();
            Pen p = new Pen(Color.BlanchedAlmond);

            int x = 0, y = 0;
            x = 25;
            y = 10;

            int xAdd = 340 / game.Lines.Count;

            foreach (LogicLine line in game.Lines)
            {
                Line l = new Line(p, x, y, x, 180);
                x += xAdd;
                drawableLines.Add(l);
            }
        }

        private void DrawApple()
        {
            drawableLines = new List<Line>();
            Pen p = new Pen(Color.BlanchedAlmond);
        }

        private void GUI_Load(object sender, EventArgs e)
        {

        }
    }
}
