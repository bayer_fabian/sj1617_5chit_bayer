﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchMe
{
    public enum EState
    {
        STOPPED = 0, RUNNING = 1, LOST = 2, WON = 3
    }
}
