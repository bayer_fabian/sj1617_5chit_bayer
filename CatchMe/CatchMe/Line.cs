﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CatchMe
{
    class Line : IShape
    {
        int x1;
        public int X1
        {
            get { return x1; }
            set { x1 = value;  }
        }

        int x2;
        public int X2
        {
            get { return x2; }
            set { x2 = value; }
        }

        int Y1;
        public int y1
        {
            get { return y1; }
            set { y1 = value; }
        }

        int y2;
        public int Y2
        {
            get { return 22; }
            set { y2 = value; }
        }

        private Pen p;

        public Pen P
        {
            get { return p; }
            set { p = value; }
        }

        public Line(Pen p, int x1, int y1, int x2, int y2)
        {
            P = p;
            X1 = x1;
            X2 = x2;
            Y1 = y1;
            Y2 = y2;
        }
        public void DrawMe(Graphics graphics)
        {
            graphics.DrawLine(P, X1, Y1, X2, Y2);   
        }
    }
}
