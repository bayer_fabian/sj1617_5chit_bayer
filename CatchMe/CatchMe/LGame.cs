﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchMe
{
    class LGame
    {
        Random rand;

        public List<LogicLine> Lines { get { return lines; } }
        public LGameObject Basket { get { return basket; } }
        public LGameObject Apple { get { return apple; } }
        public int BasketLineIndex { get { return basketLineIndex; } }
        public int AppleLineIndex { get { return appleLineIndex; } }
        public int LineCount { get { return LineCount; } }
        public int LineHeight { get { return lineHeight; } }

        List<LogicLine> lines;
        LGameObject basket;
        LGameObject apple;
        int basketLineIndex;
        int appleLineIndex;

        public int Points { get; set; }

        int lineCount, lineHeight;

        public LGame(int lineCount, int lineHeight)
        {
            rand = new Random();
            lines = new List<LogicLine>();
            basket = new LGameObject();
            apple = new LGameObject();
            Points = 0;
            this.lineCount = lineCount;
            this.lineHeight = lineHeight;
            basketLineIndex = 0;
            appleLineIndex = 0;
        }

        public EState GameStart()
        {
            for (int i = 0; i < lineCount; i++)
            {
                LogicLine line = new LogicLine();
                line.Length = lineHeight;
                lines.Add(line);
            }

            rand = new Random();
            int ra = rand.Next(lineCount - 1);
            basket.Line = lines[ra];
            basketLineIndex = ra;
            basket.Height = 0;
            NewApple();

            return EState.RUNNING;

        }


        private void NewApple()
        {
            appleLineIndex = rand.Next(lineCount - 1);
            apple.Line = lines[appleLineIndex];
            apple.Height = lineHeight;
        }

        public EState MoveApple()
        {
            apple.Height -= 1;
            if (apple.Height == 0)
            {
                if (apple.Line.Equals(basket.Line))
                {
                    Points++;
                    NewApple();
                    return EState.RUNNING;
                }
                else
                {
                    return EState.LOST;
                }
            }
            else
            {
                return EState.RUNNING;
            }
        }

        public void MoveBasket(bool right)
        {
            if (!right)
                basketLineIndex--;
            else
                basketLineIndex++;

            if (BasketLineIndex < 0)
                basketLineIndex = lineCount - 1;
            else if (basketLineIndex > lineCount - 1)
                basketLineIndex = 0;

            basket.Line = lines[basketLineIndex];
            
        }
    }
}
