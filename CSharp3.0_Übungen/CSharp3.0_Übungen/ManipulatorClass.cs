﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp3._0_Übungen
{
    public static class ManipulatorClass //Erweiterungsmethode
    {
        public static string Convertßtoss (this string data, string searchstring, string convertstring)
        {
            return data.Replace(searchstring, convertstring); //ß im string durch ss tauschen
        }

        public static int GetSum(this int digit)
        {
            int result = 0;

            while (digit > 0)
            {
                result += digit % 10;
                digit /= 10;

            }

            return result;
        }
    }

    
}
