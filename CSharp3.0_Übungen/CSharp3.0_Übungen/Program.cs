﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp3._0_Übungen
{
    class Program
    {
        static void Main(string[] args)
        {

            //Aufruf der 1.Erweiterungsmethode (ß durch ss tauschen)
            string teststring = "Weißensteiner / heiß / Straße";
            Console.WriteLine(teststring.Convertßtoss("ß", "ss"));

            //Aufruf der 2.Erweiterungsmethode (Zwischensumme einer Zahl berechnen)
            int digit = 123;
            Console.WriteLine("\nDie Ziffernsumme der Zahl " + digit + " lautet: " + digit.GetSum().ToString() + "\n");

            //Liste von 3 Students
            List<Student> students = new List<Student>
            {
                new Student {Name = "Lukas Biedermann", Age = 19 },
                new Student {Name = "Tobias Huber", Age = 18 },
                new Student {Name = "David Weichselbaum", Age = 18},
                new Student {Name = "Christopher Bruckner", Age = 25},
                new Student {Name = "Nikolaus Dräger", Age = 23}
            };

            //Sortierung der Liste mit anonymen Delegaten
            students.Sort(delegate (Student a, Student b)
            {
                if (a.Age == b.Age)
                    return a.Name.CompareTo(b.Name);

                return a.Age.CompareTo(b.Age);
            });

            foreach (Student item in students)
                Console.WriteLine(item);


            List<Student> students2 = new List<Student>
            {
                new Student {Name = "Kurz", Age = 10 },
                new Student {Name = "Moser", Age = 26 },
                new Student {Name = "Himmer", Age = 18}
            };

            Console.WriteLine("\n");

            //Sortierung der 2.Liste mit Lambda Expressions
            students2.Sort((a, b) => {
                if (a.Age == b.Age)
                return a.Name.CompareTo(b.Name);

                return a.Age.CompareTo(b.Age);
            });


            foreach (Student item in students2)
                Console.WriteLine(item);

            //Console.WriteLine("\n");

            //Alle Schüler die älter sind als 22, mithilfe von LinQ
            IEnumerable<string> studentsOver22 =
                from student in students
                where student.Age > 22
                select student.Name;

            Console.WriteLine("\n");

            foreach (string name in studentsOver22)
            {
                Console.WriteLine(name);
            }

            //kurze Schreibweise LinQ
            IEnumerable<string> studentsO22 =
                students2.Where(student => student.Age > 22).OrderBy(student => student.Name).Select(student => student.Name);

            Console.WriteLine("\n");
            foreach (string name in studentsO22)
            {
                Console.WriteLine(name);
            }
     
            Console.ReadLine();
        }
    }
}
