﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationOverload
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw1 = new Stopwatch();
            Stopwatch sw2 = new Stopwatch();

            int[] vect = new int[10];
            int[] vectSum = new int[10];
            int[] vectProd = new int[10];

            for (int i = 0; i < 10; i++)
            {
                vect[i] = 2 + i;
                vectSum[i] = 0;
                vectProd[i] = 1;
            }

            //********* Struct *********

            sw1.Start();
            MyStruct sumStruct = new MyStruct(vectSum);
            MyStruct prodStruct = new MyStruct(vectProd);
            MyStruct[] vectArray = new MyStruct[100000];

            for (int i = 0; i < vectArray.Length; i++)
            {
                vectArray[i] = new MyStruct(vect);
                sumStruct += vectArray[i];
                prodStruct *= vectArray[i];
            }

            sw1.Stop();


            //******** Class ********

            sw2.Start();
            MyClass summClass = new MyClass(vectSum);
            MyClass prodClass = new MyClass(vectProd);
            MyClass[] vectArray2 = new MyClass[100000];

            for (int i = 0; i < vectArray2.Length; i++)
            {
                vectArray2[i] = new MyClass(vect);
                summClass += vectArray2[i];
                prodClass *= vectArray2[i];
            }

            sw2.Stop();

            Console.WriteLine($"Structs: {sw1.ElapsedMilliseconds}");
            Console.WriteLine($"Classes: {sw2.ElapsedMilliseconds}");
            Console.ReadLine();
        }
    }
}
