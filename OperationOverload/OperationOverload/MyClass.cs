﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationOverload
{
    class MyClass
    {
        public int[] vector { get; set; }

        public MyClass(int[] vector)
        {
            this.vector = vector;
        }

        public static MyClass operator +(MyClass op1, MyClass op2)
        {
            int[] sum = new int[op1.vector.Length];

            for (int i = 0; i < sum.Length; i++)
            {
                sum[i] = op1.vector[i] + op2.vector[i];

            }

            return new MyClass(sum);
        }

        public static MyClass operator *(MyClass op1, MyClass op2)
        {
            int[] prod = new int[op1.vector.Length];

            for (int i = 0; i < prod.Length; i++)
            {
                prod[i] = op1.vector[i] + op2.vector[i];

            }

            return new MyClass(prod);
        }
    }
}
