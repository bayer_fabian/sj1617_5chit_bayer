﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationOverload
{
    struct MyStruct
    {
        public int[] vector { get; set; }

        public MyStruct(int[] vector)
        {
            this.vector = vector;
        }

        public static MyStruct operator +(MyStruct op1, MyStruct op2)
        {
            int[] sum = new int[op1.vector.Length];

            for (int i = 0; i < sum.Length; i++)
            {
                sum[i] = op1.vector[i] + op2.vector[i];

            }

            return new MyStruct(sum);
        }

        public static MyStruct operator *(MyStruct op1, MyStruct op2)
        {
            int[] prod = new int[op1.vector.Length];

            for (int i = 0; i < prod.Length; i++)
            {
                prod[i] = op1.vector[i] + op2.vector[i];

            }

            return new MyStruct(prod);
        }

    }
}
