﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using HangmanLogic;

namespace Hangman_Client
{
    class Program
    {

        static Logik logik = new Logik();
        static Word Word;
        static Stream s;
        public static void Main(string[] args)
        {
            TcpClient client = new TcpClient("localhost", 2055);
            try
            {
                CreateWord();
                s = client.GetStream();
                while (true)
                {
                    string name = Console.ReadLine();
                    WriteSyncAsync(client, name);
                    string xy = Read(client);
                    CreateGame(xy);
                }
            }
            finally
            {
                client.Close();
            }
        }
        private static void CreateGame(string serialized)
        {
            Word = logik.DeSerialize(serialized);
            Console.Clear();
            for (int i = 0; i < Word.Name.Length; i++)
            {
                if (Word.guesses.Exists(delegate(Guess a) { if (a.Letter == Word.Name[i].ToString() && a.Right) return true; else return false; }))
                    Console.Write(Word.Name[i] + " ");
                else
                    Console.Write("__ ");
            }
            Console.WriteLine("\nLifes left: " + Word.Life);
            switch (logik.GetCheckWon(logik.Serialize(Word)))
            {
                case State.WON:
                    Console.WriteLine("You won the game!!");
                    break;
                case State.LOST:
                    Console.WriteLine("You lost the game!!");
                    break;
                case State.NEUTRAL:
                    Console.WriteLine("The game is continued");
                    break;
                default:
                    break;
            }
            Console.Write("Input: ");
        }
        private static void WriteSyncAsync(TcpClient client, string name)
        {
            StreamWriter sw = new StreamWriter(s);
            sw.AutoFlush = true;
            Word.AddGuess(new Guess(name, false));

            sw.WriteLine(logik.Serialize(Word));

        }

        private static void CreateWord()
        {
            string word = logik.GetWord();
            Word = new Word(word);
            for (int i = 0; i < word.Length; i++)
            {
                Console.Write("_ ");
            }
            Console.WriteLine("\nLifes left: " + Word.Life);
            Console.Write("Input: ");
        }

        public static string Read(TcpClient client)
        {
            StreamReader sr = new StreamReader(s);
            return sr.ReadLine();
        }
    }
}
