﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace HangmanLogic
{

    public class Logik
    {
        XmlSerializer serializer;
        StringWriter stringWriter;

        public Logik()
        {
        }


        public Word DeSerialize(string serial)
        {

            serializer = new XmlSerializer(typeof(Word));
            Word result;

            using (TextReader reader = new StringReader(serial))
            {
                result = (Word)serializer.Deserialize(reader);
            }

            return result;
        } //from string to object
        public string Serialize(Word o)
        {

            serializer = new XmlSerializer(typeof(Word));
            stringWriter = new StringWriter();
            using (var writer = XmlWriter.Create(stringWriter, new XmlWriterSettings() { Indent = false, ConformanceLevel = ConformanceLevel.Fragment, NewLineHandling = NewLineHandling.None, Encoding = Encoding.UTF8 }))
            {
                writer.WriteWhitespace("");
                serializer.Serialize(writer, o);
            }

            return stringWriter.ToString();
        } //from object to string
        public string GetWord()
        {
            List<string> list = new List<string>();
            string[] getit;
            using (StreamReader sr = new StreamReader("words.txt"))
            {
                while (sr.Peek() != -1)
                {
                    getit = sr.ReadLine().Split(';');
                    list.AddRange(getit);

                }
            }
            Random rand = new Random();


            return list[rand.Next(0, list.Count)];
        }

        public State CheckWon(string serial)
        {
            Word myWord = this.DeSerialize(serial);

            if (myWord.Life <= 0)
                return State.LOST;

            for (int i = 0; i < myWord.Name.Length; i++)
            {
                if (!myWord.guesses.Exists(delegate(Guess a) { if (a.Letter == myWord.Name[i].ToString() && a.Right) return true; else return false; }))
                    return State.NEUTRAL;
            }
            return State.WON;
        }
    }
}
