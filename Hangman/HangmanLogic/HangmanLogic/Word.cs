﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangmanLogic
{
    [Serializable]
    public class Word
    {
        public string Name { get; set; }
        public List<Guess> guesses;
        public int ID { get; set; }
        public int Life { get; set; }
        public Word()
        {

        }
        public Word(string name)
        {
            Name = name;
            guesses = new List<Guess>();
            Life = 10;
        }
        public bool AddGuess(Guess g)
        {
            if (guesses.Exists(delegate(Guess b) { if (b == g)return true; else return false; }))
                return false;

            guesses.Add(g);
            return true;
        }
    }
}
