﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangmanLogic
{
    [Serializable]
    public class Guess
    {
        public string Letter { get; set; }
        public bool Right { get; set; }
        public Guess()
        {

        }
        public Guess(string c, bool guessed)
        {
            Letter = c;
            Right = guessed;
        }
    }
}
