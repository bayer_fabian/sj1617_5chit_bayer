﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using HangmanLogic;


namespace EmployeeTcpServer
{
    class Program
    {

        static TcpListener listener;

        static XmlSerializer serializer;
        static Logik logik;
        public static void Main()
        {
            listener = new TcpListener(IPAddress.Any, 2055);
            listener.Start();
            Console.WriteLine("Server mounted, listening to port 2055");
            try
            {
                while (true)
                {
                    Socket soc = listener.AcceptSocket();
                    Thread t = new Thread(Service);
                    t.Start(soc);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Failed to connect");
            }
        }
        public static void Service(object socket)
        {
            logik = new Logik();
            Socket soc = (Socket)socket;
            Word word = new Word();
            Console.WriteLine("Connected: {0} ", soc.RemoteEndPoint);
            serializer = new XmlSerializer(typeof(Word));
            try
            {
                Stream s = new NetworkStream(soc);
                StreamReader sr = new StreamReader(s);
                StreamWriter sw = new StreamWriter(s);
                sw.AutoFlush = true; // enable automatic flushing


                while (true) 
                {
                    string getString = sr.ReadLine();
                    word = logik.DeSerialize(getString);

                    if (word.Name.Contains(word.guesses[word.guesses.Count - 1].Letter))
                        word.guesses[word.guesses.Count - 1].Right = true;
                    else
                        word.Life--;

                    sw.WriteLine(logik.Serialize(word));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Disconnected: " + soc.RemoteEndPoint);
            }
            soc.Close();

        }
    }
}
