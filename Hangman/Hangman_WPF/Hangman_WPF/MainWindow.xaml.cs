﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HangmanLogic;
using System.IO;
using System.Net.Sockets;

namespace Hangman_WPF
{

    public partial class MainWindow : Window
    {
        Logik logik = new Logik();
        Word Word;
        Stream s;
        StreamWriter sw;
        StreamReader sr;
        public MainWindow()
        {
            InitializeComponent();
            StartIt();
        }
        private void StartIt()
        {

            try
            {
                CreateWord();

                TcpClient client = new TcpClient("localhost", 2055);
                s = client.GetStream();
                sr = new StreamReader(s);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        private void CreateGame(string serialized)
        {
            Word = logik.DeSerialize(serialized);
            lblfullWord.Content = "";
            for (int i = 0; i < Word.Name.Length; i++)
            {
                if (Word.guesses.Exists(delegate(Guess a) { if (a.Letter == Word.Name[i].ToString() && a.Right) return true; else return false; }))
                    lblfullWord.Content += Word.Name[i] + " ";
                else
                    lblfullWord.Content += "_ ";
            }
            lblLifes.Content = "Life: " + Word.Life;
            switch (logik.GetCheckWon(logik.Serialize(Word)))
            {
                case State.WON:
                    MessageBox.Show("You won the game!");
                    break;
                case State.LOST:
                    MessageBox.Show("You lost the game!!");
                    break;
                default:
                    break;
            }
        }
        private void WriteSyncAsync(string name)
        {
            Word.AddGuess(new Guess(name, false));

            sw = new StreamWriter(s)
            {
                AutoFlush = true
            };

            sw.WriteLine(logik.Serialize(Word));


        }

        private void CreateWord()
        {
            string word = logik.GetWord();
            Word = new Word(word);
            for (int i = 0; i < word.Length; i++)
            {
                lblfullWord.Content += "_ ";
            }
        }

        public async Task Read()
        {
            
            string x = await this.sr.ReadLineAsync();
            
            CreateGame(x);
        }

        private void btnRequest_Click(object sender, RoutedEventArgs e)
        {
            WriteSyncAsync(txtLetter.Text);
            Read();
        }
    }
}
