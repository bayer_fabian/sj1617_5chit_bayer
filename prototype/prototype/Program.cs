﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iText.Kernel.Events;
using iText.Kernel.Pdf;
using iText.Layout.Element;
using System.IO;
using iText.Layout;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Font;
using iText.Kernel.Pdf;
using iText.IO.Image;
using iText.Kernel.Pdf.Xobject;
using iText.Kernel.Colors;
using iText.Kernel.Geom;

namespace prototype
{
    class Program
    {

        static void Main(string[] args)
        {
            PdfWriter writer = new PdfWriter("test.pdf");
            PdfDocument pdf = new PdfDocument(writer);
            
            pdf.AddEventHandler(PdfDocumentEvent.END_PAGE, new MyEventHandler());
            Document document = new Document(pdf);

            Table table = new Table(8);
            for (int i = 0; i < 16; i++)
            {
                table.AddCell("Tabelle Test");
            }
            document.Add(table);


            Image image = new Image(ImageDataFactory.Create("test.jpg"));
            document.Add(image);

            Paragraph p = new Paragraph("Text mittig");
            p.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);

            Paragraph p1 = new Paragraph("Text rechts");
            p1.SetTextAlignment(iText.Layout.Properties.TextAlignment.RIGHT);

            document.Add(p);
            document.Add(p1);

            document.Add(new Paragraph("Test Text"));
            document.Close();


        }
    }
}
