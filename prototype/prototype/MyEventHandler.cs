﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iText.Kernel.Events;
using iText.Kernel.Pdf;
using iText.Layout.Element;
using System.IO;
using iText.Layout;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Font;
using iText.Kernel.Pdf;
using iText.IO.Font;
using iText.Kernel.Colors;
using iText.Kernel.Geom;
namespace prototype
{
    class MyEventHandler : IEventHandler
    {
    

        public virtual void HandleEvent(Event @event)
        {
            PdfDocumentEvent docEvent = (PdfDocumentEvent)@event;
            PdfDocument pdfDoc = docEvent.GetDocument();
            PdfPage page = docEvent.GetPage();
            int pageNumber = pdfDoc.GetPageNumber(page);
            Rectangle pageSize = page.GetPageSize();
            PdfCanvas pdfCanvas = new PdfCanvas(page.NewContentStreamBefore(), page.GetResources(), pdfDoc);

            //Header
            pdfCanvas.BeginText()
                .SetFontAndSize(PdfFontFactory.CreateFont(FontConstants.COURIER), 12)
                .MoveText(pageSize.GetWidth() / 2 - 60, pageSize.GetTop() - 20)
                .ShowText("THE TRUTH IS OUT THERE")
                .MoveText(60, -pageSize.GetTop() + 30)
                .ShowText("FOOTER")
                .EndText();
            pdfCanvas.Release();
        }

    }
}
