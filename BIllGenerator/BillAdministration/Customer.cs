﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillAdministration
{
    class Customer
    {
        public int customerID { get; set; }

        public string Name { get; set; }

        public string ZIP { get; set; }

        public string Housenumber { get; set; }

        public string City { get; set; }

        public string VATID { get; set; }

        public string Street { get; set; }

    }
}
