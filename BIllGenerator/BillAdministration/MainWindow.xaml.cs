﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Web;

using System.Diagnostics;

namespace BillAdministration
{

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            dbconnect.DbConnect();
            bills = dbconnect.fetchBills();
            billbox.ItemsSource = bills;
            
            
        }
        List<Bill> bills;
        public double sum = 0;
        ConnectDB dbconnect = new ConnectDB();


        private void button_Click(object sender, RoutedEventArgs e)
        {
            Bill b = (Bill)billbox.SelectedItem;
            
            Document billdoc = new Document(PageSize.A4, 25, 25, 30, 30);
            PdfWriter billwriter = PdfWriter.GetInstance(billdoc, new FileStream("Rechnung"+b.billID.ToString()+".pdf", FileMode.Create));

            billdoc.Open();

            PdfPTable tbl = new PdfPTable(2);
            tbl.WidthPercentage = 100;
         

            PdfPCell cellLogo = new PdfPCell();
            cellLogo.BorderWidth = PdfPCell.NO_BORDER;

            PdfPTable customertable = new PdfPTable(1);
            customertable.WidthPercentage = 100;
            customertable.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPTable billhead = new PdfPTable(2);
            billhead.WidthPercentage = 100;

            PdfPTable space = new PdfPTable(1);
            space.WidthPercentage = 100;

            PdfPTable PriceAndTax = new PdfPTable(1);
            PriceAndTax.WidthPercentage = 100;
            PriceAndTax.HorizontalAlignment = Element.ALIGN_RIGHT;

            PdfPTable Message = new PdfPTable(1);
            Message.WidthPercentage = 60;
            Message.HorizontalAlignment = Element.ALIGN_MIDDLE;

            PdfPTable Banktable = new PdfPTable(1);
            Banktable.WidthPercentage = 60;
            Banktable.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPTable Footertable = new PdfPTable(1);
            Footertable.WidthPercentage = 50;
            Footertable.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            
          
            





            if (System.IO.File.Exists(@b.company.Logo))
            {
                iTextSharp.text.Image img1 = iTextSharp.text.Image.GetInstance(@b.company.Logo);
                img1.ScaleAbsolute(new iTextSharp.text.Rectangle(150, 150));
                cellLogo.AddElement(img1);
            }

            tbl.AddCell(cellLogo);

            // COMPANY
            var parCompanyName = new iTextSharp.text.Paragraph(b.company.Name);
            parCompanyName.SetLeading(0, 1.2f);
            parCompanyName.Alignment = Element.ALIGN_RIGHT;

            var parCompanyStreetAndHousenumber = new iTextSharp.text.Paragraph(b.company.Street+ " "+ b.company.Housenumber);
            parCompanyStreetAndHousenumber.SetLeading(0, 1.2f);
            parCompanyStreetAndHousenumber.Alignment = Element.ALIGN_RIGHT;

            var parCompanyPostalcodeAndCity = new iTextSharp.text.Paragraph(b.company.ZIP+ " " + b.company.City);
            parCompanyPostalcodeAndCity.SetLeading(0, 1.2f);
            parCompanyPostalcodeAndCity.Alignment = Element.ALIGN_RIGHT;

            var parCompanyPhone = new iTextSharp.text.Paragraph("Tel: "+b.company.Phone);
            parCompanyPhone.SetLeading(0, 1.2f);
            parCompanyPhone.Alignment = Element.ALIGN_RIGHT;

            var parCompanyMobile = new iTextSharp.text.Paragraph(b.company.Email);
            parCompanyMobile.SetLeading(0, 1.2f);
            parCompanyMobile.Alignment = Element.ALIGN_RIGHT;

            PdfPCell cellCompany = new PdfPCell();
            cellCompany.AddElement(parCompanyName);
            cellCompany.AddElement(parCompanyStreetAndHousenumber);
            cellCompany.AddElement(parCompanyPostalcodeAndCity);
            cellCompany.AddElement(parCompanyPhone);
            cellCompany.AddElement(parCompanyMobile);
            cellCompany.BorderWidth = PdfPCell.NO_BORDER;
            tbl.AddCell(cellCompany);

            //CUSTOMER
            var parCustomerFirma = new iTextSharp.text.Paragraph("Firma");
            parCustomerFirma.SetLeading(0, 1.2f);
            parCustomerFirma.Alignment = Element.ALIGN_LEFT;

            var parCustomerName = new iTextSharp.text.Paragraph(b.customer.Name);
            parCustomerName.SetLeading(0, 1.2f);
            parCustomerName.Alignment = Element.ALIGN_LEFT;

            var parCustomerPostalcodeAndCity = new iTextSharp.text.Paragraph(b.customer.ZIP + " " + b.customer.City);
            parCustomerPostalcodeAndCity.SetLeading(0, 1.2f);
            parCustomerPostalcodeAndCity.Alignment = Element.ALIGN_LEFT;

            var parCustomerStreetAndHousenumber = new iTextSharp.text.Paragraph(b.customer.Street + " " + b.customer.Housenumber);
            parCustomerStreetAndHousenumber.SetLeading(0, 1.2f);
            parCustomerStreetAndHousenumber.Alignment = Element.ALIGN_LEFT;

            
            PdfPCell cellcustomer = new PdfPCell();
            cellcustomer.BorderWidth = PdfPCell.NO_BORDER;
            cellcustomer.AddElement(parCustomerFirma);
            cellcustomer.AddElement(parCustomerName);
            cellcustomer.AddElement(parCustomerPostalcodeAndCity);
            cellcustomer.AddElement(parCustomerStreetAndHousenumber);
            customertable.AddCell(cellcustomer);

            //RECHNUNG HEAD
            var parBillnmbr = new iTextSharp.text.Paragraph("Rechnung: " + b.billID);
            parBillnmbr.SetLeading(0, 1.2f);
            parBillnmbr.Alignment = Element.ALIGN_LEFT;

            //Rechnung HEAD-Right
            var parUID = new iTextSharp.text.Paragraph("UID: " + b.customer.VATID);
            parUID.SetLeading(0, 1.2f);
            parUID.Alignment = Element.ALIGN_RIGHT;

            var parcustomernumber = new iTextSharp.text.Paragraph("Kundennummer: " + b.customer.customerID);
            parcustomernumber.SetLeading(0, 1.2f);
            parcustomernumber.Alignment = Element.ALIGN_RIGHT;


            var parOrderDate = new iTextSharp.text.Paragraph("Bestelldatum: " + b.order_date);
            parOrderDate.SetLeading(0, 1.2f);
            parOrderDate.Alignment = Element.ALIGN_RIGHT;

            var parDeliveryDate = new iTextSharp.text.Paragraph("Lieferdatum: " + b.delivery_date);
            parDeliveryDate.SetLeading(0, 1.2f);
            parDeliveryDate.Alignment = Element.ALIGN_RIGHT;

            PdfPCell billnmbr = new PdfPCell();
            billnmbr.BorderWidth = PdfPCell.NO_BORDER;
            billnmbr.AddElement(parBillnmbr);

            PdfPCell billNumbersAndDate = new PdfPCell();
            billNumbersAndDate.BorderWidth = PdfPCell.NO_BORDER;
            billNumbersAndDate.AddElement(parUID);
            billNumbersAndDate.AddElement(parcustomernumber);
            billNumbersAndDate.AddElement(parOrderDate);
            billNumbersAndDate.AddElement(parDeliveryDate);

            billhead.AddCell(billnmbr);
            billhead.AddCell(billNumbersAndDate);

            PdfPCell spacecell = new PdfPCell();
            spacecell.BorderWidth = PdfPCell.NO_BORDER;
            spacecell.FixedHeight = 40f;

            space.AddCell(spacecell);



            PdfPTable tblProducts = new PdfPTable(6);
            tblProducts.WidthPercentage = 100;
            tblProducts.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell[] cellHeadings = new PdfPCell[]
            {
                new PdfPCell(new Phrase("Menge")),
                new PdfPCell(new Phrase("Einheit")),
                new PdfPCell(new Phrase("Bezeichnung")),
                new PdfPCell(new Phrase("Rabatt")),
                new PdfPCell(new Phrase("Einzelpreis")),
                new PdfPCell(new Phrase("Gesamtpreis")),

            };
            PdfPRow rowHeadings = new PdfPRow(cellHeadings);

            tblProducts.Rows.Add(rowHeadings);



            foreach (Product item in b.product)
            {
                PdfPCell[] cellsRow0 = new PdfPCell[]
                {
                    new PdfPCell(new Phrase(item.amount.ToString())),
                    new PdfPCell(new Phrase(item.unit)),
                    new PdfPCell(new Phrase(item.description)),
                    new PdfPCell(new Phrase(item.discount.ToString())),
                    new PdfPCell(new Phrase(item.price.ToString("0.00"))),
                    new PdfPCell(new Phrase((item.amount * item.price).ToString("0.00"))),
                };
                PdfPRow row0 = new PdfPRow(cellsRow0);
                sum += (item.amount * item.price);
                tblProducts.Rows.Add(row0);
            }

            //Rechnung Preis & Tax
            var parSum = new iTextSharp.text.Paragraph("Gesamtbetrag (exkl. USt:    " + sum.ToString("0.00"));
            parSum.SetLeading(0, 1.2f);
            parSum.Alignment = Element.ALIGN_RIGHT;

            var parTax = new iTextSharp.text.Paragraph("+ 20%:    " + (sum * 0.2).ToString("0.00"));
            parTax.SetLeading(0, 1.2f);
            parTax.Alignment = Element.ALIGN_RIGHT;

            var parSumWithTax = new iTextSharp.text.Paragraph("Gesamtbetrag (inkl. USt:    " + (sum * 1.2).ToString("0.00"));
            parSumWithTax.SetLeading(0, 1.2f);
            parSumWithTax.Alignment = Element.ALIGN_RIGHT;

            PdfPCell priceandtax = new PdfPCell();
            priceandtax.BorderWidth = PdfPCell.NO_BORDER;
            priceandtax.AddElement(parSum);
            priceandtax.AddElement(parTax);
            priceandtax.AddElement(parSumWithTax);

            PriceAndTax.AddCell(priceandtax);

            //Nachricht
            var parMsg   = new iTextSharp.text.Paragraph("                                        Kurz bedankt sich für Ihren Einkauf");
            parMsg.SetLeading(0, 1.2f);
            parMsg.Alignment = Element.ALIGN_JUSTIFIED_ALL;

            PdfPCell messagecell = new PdfPCell();
            messagecell.BorderWidth = PdfPCell.NO_BORDER;
            messagecell.AddElement(parMsg);
            Message.AddCell(messagecell);


            //BANKDATEN
            var parBank = new iTextSharp.text.Paragraph(b.company.Bank);
            parBank.SetLeading(0, 1.2f);
            parBank.Alignment = Element.ALIGN_LEFT;

            var parIBAN = new iTextSharp.text.Paragraph(b.company.IBAN);
            parIBAN.SetLeading(0, 1.2f);
            parIBAN.Alignment = Element.ALIGN_LEFT;

            var parUIDcompany = new iTextSharp.text.Paragraph(b.company.Bank);
            parUIDcompany.SetLeading(0, 1.2f);
            parUIDcompany.Alignment = Element.ALIGN_LEFT;

            PdfPCell bankcell = new PdfPCell();
            bankcell.BorderWidth = PdfPCell.NO_BORDER;
            bankcell.AddElement(parBank);
            bankcell.AddElement(parIBAN);
            bankcell.AddElement(parUIDcompany);
            Banktable.AddCell(bankcell);


            //FOOTER
            var parFooter = new iTextSharp.text.Paragraph("                                            Rechnung © Kurz Inc.");
            parBank.SetLeading(0, 1.2f);
            parBank.Alignment = Element.ALIGN_JUSTIFIED_ALL;

            PdfPCell footercell = new PdfPCell();
            footercell.BorderWidth = PdfPCell.NO_BORDER;
            footercell.VerticalAlignment = Element.ALIGN_BOTTOM;
            footercell.AddElement(parFooter);

            Footertable.AddCell(footercell);



            // ADD TABLES
            billdoc.Add(tbl);
            billdoc.Add(customertable);
            billdoc.Add(space);
            billdoc.Add(billhead);
            billdoc.Add(space);
            billdoc.Add(tblProducts);
            billdoc.Add(PriceAndTax);
            billdoc.Add(space);
            billdoc.Add(space);
            billdoc.Add(Message);
            billdoc.Add(space);
            billdoc.Add(Banktable);

            billdoc.Add(space);
            billdoc.Add(space);
            billdoc.Add(Footertable);


            billdoc.Close();
            billwriter.Close();

            if (File.Exists("Rechnung" + b.billID.ToString() + ".pdf"))
                System.Diagnostics.Process.Start("Rechnung" + b.billID.ToString() + ".pdf");
            else
                MessageBox.Show("generieren von PDF ist fehlgeschlagen");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ConnectDB connector = new ConnectDB();
            connector.DbConnect();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (billbox.SelectedIndex > -1)
            {
                button.IsEnabled = true;
            }
            else
                button.IsEnabled = false;
        }
    }
}
