﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MySql.Data;
using MySql.Data.MySqlClient;


namespace BillAdministration
{
    class ConnectDB
    {
        //string connectionstring = null;
        string connectionstring = "SERVER=localhost;" + "DATABASE=billdb;" + "UID=root;" + "PASSWORD=;";
        MySqlConnection connection; 

        public void DbConnect()
        {
            
       //     MySqlCommand command = connection.CreateCommand();

            connection = new MySqlConnection(connectionstring);
            try
            {

                connection.Open();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not open connection" + ex);
                
                throw;
            }
        }

        public List<Bill> fetchBills()
        {
            MySqlCommand testcmd = new MySqlCommand("SELECT * FROM bills b JOIN customer c ON b.customerID = c.customerID JOIN company co ON b.Company_companyID = co.companyID", connection);
            MySqlDataReader reader = testcmd.ExecuteReader();
            //SELECT* FROM products p JOIN products_bill pb ON p.productID = pb.Products_productID WHERE pb.Bills_billID =
            List <Bill> bills = new List<Bill>();
            if (reader.HasRows)
            {

                while (reader.Read())
                {

                    Customer cu = new Customer();
                    cu.customerID = reader.GetInt32(7);
                    cu.Name = reader.GetString(8);
                    cu.ZIP = reader.GetString(9);
                    cu.Housenumber = reader.GetString(10);
                    cu.City = reader.GetString(11);
                    cu.VATID = reader.GetString(12);
                    cu.Street = reader.GetString(13);

                    Company co = new Company();
                    co.Id = reader.GetInt32(14);
                    co.Name = reader.GetString(15);
                    co.ZIP = reader.GetString(16);
                    co.Housenumber = reader.GetString(17);
                    co.City = reader.GetString(18);
                    co.Street = reader.GetString(19);
                    co.VATID = reader.GetString(20);
                    co.Email = reader.GetString(21);
                    co.Phone = reader.GetString(22);
                    co.IBAN = reader.GetString(23);
                    co.Bank = reader.GetString(24);
                    co.Logo = reader.GetString(25);

                    Bill b = new Bill();
                    b.billID = reader.GetInt32(0);
                    b.delivery_date = reader.GetDateTime(1);
                    b.order_date = reader.GetDateTime(2);
                    b.customer = cu;
                    b.slogan = reader.GetString(4);
                    b.company = co;
                    b.tax = reader.GetInt32(6);

                    bills.Add(b);
                }
            }
            reader.Close();

            for (int i = 0; i < bills.Count; i++)
            {
                MySqlCommand testcmd2 = new MySqlCommand("SELECT * FROM products p JOIN products_bill pb ON p.productID = pb.Products_productID WHERE pb.Bills_billID = " + bills[i].billID, connection);
                MySqlDataReader reader2 = testcmd2.ExecuteReader();

                List<Product> products = new List<Product>();
                if (reader2.HasRows)
                {

                    while (reader2.Read())
                    {

                        Product p = new Product();
                        p.productID = reader2.GetInt32(0);
                        p.unit = reader2.GetString(1);
                        p.description = reader2.GetString(2);
                        p.discount = reader2.GetInt32(3);
                        p.price = reader2.GetDouble(4);
                        p.amount = reader2.GetDouble(7);
                        products.Add(p);
                    }
                }
                reader2.Close();
                bills[i].product = products;
            }
            return bills;

        }

        public void CloseConnection()
        {
            connection.Close();
        }
    }
}
