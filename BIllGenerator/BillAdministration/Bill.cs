﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillAdministration
{
    class Bill
    {
        public int billID { get; set; }

        public DateTime delivery_date { get; set; }

        public DateTime order_date { get; set; }

        public Customer customer { get; set; }

        public string slogan { get; set; }

        public Company company { get; set; }

        public int tax { get; set; }

        public List<Product> product { get; set; }

        public override string ToString()
        {
            return "Company: "+billID + " - " + "order date: "+order_date;
        }
    }
}
