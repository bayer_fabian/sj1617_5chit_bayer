﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillAdministration
{
    class Product
    {
        public int productID { get; set; }

        public string unit { get; set; }

        public string description { get; set; }

        public int discount { get; set; }

        public double price { get; set; }

        public double amount { get; set; }

        /*
        public string Menge { get; set; }
        public string Einheit { get; set; }
        public string Bezeichnung { get; set; }
        public string Rabatt { get; set; }
        public string Einzelpreis { get; set; }
        public string Gesamtpreis { get; set; }

        public Product(string Menge, string Einheit, string Bezeichnung, string Rabatt, string Einzelpreis, string Gesamtpreis)
        {
            this.Menge = Menge;
            this.Einheit = Einheit;
            this.Bezeichnung = Bezeichnung;
            this.Rabatt = Rabatt;
            this.Einzelpreis = Einzelpreis;
            this.Gesamtpreis = Gesamtpreis;
        }*/

        public override string ToString()
        {
            return productID + ": " + description;
        }
    }
}
