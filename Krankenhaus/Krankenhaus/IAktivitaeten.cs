﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krankenhaus
{
    public interface IAktivitaeten
    {
        void Operieren();

        void PatientenAufnehmen();
    }
}
