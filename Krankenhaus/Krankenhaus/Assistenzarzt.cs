﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krankenhaus
{
    public class Assistenzarzt : Arzt, IAktivitaeten
    {
        public bool AusbildungAbgeschlossen { get; set; }

        public Assistenzarzt(int id, string vorname, string nachname, bool ausbildungabgeschlossen) :base(id, vorname, nachname)
        {
            AusbildungAbgeschlossen = ausbildungabgeschlossen;
        }

        public void Operieren()
        {
            throw new NotImplementedException();
        }

        public void PatientenAufnehmen()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return "Ass. Arzt: " + base.ToString() + " [" + (AusbildungAbgeschlossen ? "fertig" : "in Ausbildung") + "]";
        }
    }
}
