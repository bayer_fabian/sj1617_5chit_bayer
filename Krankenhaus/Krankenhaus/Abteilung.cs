﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krankenhaus
{
    public class Abteilung
    {
        public string Bezeichnung { get; set; }
        public int ID { get; set; }

        public List<Arzt> arztListe = new List<Arzt>();

        public Abteilung(int id, string bezeichnung)
        {
            Bezeichnung = bezeichnung;
            ID = id;
        }

        public void arztHinzufuegen(Arzt a)
        {
            arztListe.Add(a);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Abteilung: "+Bezeichnung);
            sb.Append("\nÄrzteteam: \n------------------------\n");

            arztListe.Sort(delegate (Arzt a, Arzt b) {
                if (a is LeitenderArzt)
                {
                    if (b is LeitenderArzt)
                    {
                        LeitenderArzt a0 = (LeitenderArzt)a;
                        LeitenderArzt b0 = (LeitenderArzt)b;

                        return (((int)a0.Funk).CompareTo((int)b0.Funk));
                    }
                    else
                        return -1;
                }
                else if (a is Assistenzarzt)
                {
                    if (b is LeitenderArzt)
                        return 1;
                    else if (b is Assistenzarzt)
                        return 0;
                    else
                        return -1;
                }
                else
                {
                    if (b is LeitenderArzt | b is Assistenzarzt)
                        return 1;
                    else
                        return 0;
                }
            });

            foreach (Arzt a in arztListe)
            {
                sb.Append(a.ToString() + "\n");
            }
            return sb.ToString();
        }
    }
}
