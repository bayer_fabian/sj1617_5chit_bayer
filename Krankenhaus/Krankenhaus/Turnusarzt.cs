﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krankenhaus
{
    public class Turnusarzt : Arzt
    {
        public int Dauer { get; set; }

        public Turnusarzt(int id, string vorname, string nachname, int dauer) : base(id, vorname, nachname)
        {
            Dauer = dauer;
        }

        public override string ToString()
        {
            return "Turnusarzt: " + base.ToString() + " [" + Dauer + " Monate" + "]";
        }   
    }
}
