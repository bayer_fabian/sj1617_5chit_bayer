﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krankenhaus
{
    public abstract class Arzt
    {
        public int ID { get; set; }
        public string Nachname { get; set; }
        public string Vorname { get; set; }

        protected List<DateTime> visiten; 

        public Arzt(int id, string vorname, string nachname)
        {
            visiten = new List<DateTime>();
            ID = id;
            Vorname = vorname;
            Nachname = nachname;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Nachname + " " + Vorname);

            if (visiten.Count > 0)
                sb.Append(" - Visiten: ");
            foreach (DateTime v in visiten)
            {
                sb.Append(v.ToShortDateString() + " ");
            }

            return sb.ToString();
        }

        public void Visitieren(DateTime d)
        {
            visiten.Add(d);
        }

    }
}
