﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krankenhaus
{
   public class LeitenderArzt : Arzt, IAktivitaeten
    {
        private Funktion funk;

        public Funktion Funk
        {
            get { return funk; }
            set { funk = value; }
        }


        public LeitenderArzt(int id, string vorname, string nachname, Funktion funktion) : base(id, vorname, nachname)
        {
            ID = id;
            Vorname = vorname;
            Nachname = nachname;
            this.funk = funktion;
        }

        public void Operieren()
        {
            throw new NotImplementedException();
        }

        public void PatientenAufnehmen()
        {

        }

        public override string ToString()
        {
            return funk + ": " + base.ToString();
        }

    }
}
