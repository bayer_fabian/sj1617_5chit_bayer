﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Krankenhaus;

namespace KrankenhausTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAbteilung()
        {
            Abteilung actual = new Abteilung(1, "TestAbteilung");
            actual.arztHinzufuegen(new Assistenzarzt(1, "Sebastian", "Weißensteiner", true));

            Assert.AreEqual(1, actual.ID);
            Assert.AreEqual("TestAbteilung", actual.Bezeichnung);
            Assert.IsNotNull(actual.arztListe);
        }

        [TestMethod]
        public void TestLeitenderArztToString()
        {
            string expectedvalue = "Oberarzt: Fabian Bayer";
            LeitenderArzt leitenderArzt = new LeitenderArzt(1, "Fabian", "Bayer", Funktion.Oberarzt);

            string actualvalue = leitenderArzt.ToString();
            Assert.AreEqual(expectedvalue, actualvalue);
        }

        [TestMethod]
        public void TestAssistenzarztToString()
        {
            string expectedvalue = "Ass. Arzt: Nikolaus Dräger [fertig]";
            Assistenzarzt assistenzArzt = new Assistenzarzt(1, "Nikolaus", "Dräger", true);

            string actualvalue = assistenzArzt.ToString();
            Assert.AreEqual(expectedvalue, actualvalue);
        }

        [TestMethod]
        public void TestTurnusarztToString()
        {
            string expectedvalue = "Turnusarzt: K. Franz Josef [10 Monate]";
            Turnusarzt turnusArzt = new Turnusarzt(1, "Josef", "K. Franz", 10);
                
            string actualvalue = turnusArzt.ToString();
            Assert.AreEqual(expectedvalue, actualvalue);
        }

        [TestMethod]
        public void TestOutput()
        {
            string expected = "Abteilung: HNO\nÄrzteteam: \n------------------------\n" +
                                    "Primarius: Jünger Ferdinand\n" +
                                    "Oberarzt: Hogan Stephanie - Visiten: 12.12.2012 13.12.2012 \n" +
                                    "Oberarzt_Stv: Ganz Edith\n" +
                                    "Ass. Arzt: Mayer Isabella [fertig]\n" +
                                    "Ass. Arzt: Hochmut Werner [in Ausbildung]\n" +
                                    "Turnusarzt: Turner Anton [10 Monate]\n" +
                                    "Turnusarzt: Turing Theresa - Visiten: 12.12.2012 13.12.2012  [6 Monate]\n" +
                                    "Turnusarzt: Azubius Fritz [12 Monate]\n";

            Abteilung hno = new Abteilung(1, "HNO");
            Turnusarzt t1 = new Turnusarzt(201, "Anton", "Turner", 10);
            Turnusarzt t2 = new Turnusarzt(202, "Theresa", "Turing", 6);
            t2.Visitieren(DateTime.Parse("12.12.2012"));
            t2.Visitieren(DateTime.Parse("13.12.2012"));
            Turnusarzt t3 = new Turnusarzt(203, "Fritz", "Azubius", 12);
            LeitenderArzt prim = new LeitenderArzt(4711, "Ferdinand", "Jünger", Funktion.Primarius);
            LeitenderArzt ober = new LeitenderArzt(815, "Stephanie", "Hogan", Funktion.Oberarzt);
            LeitenderArzt oberstv = new LeitenderArzt(915, "Edith", "Ganz", Funktion.Oberarzt_Stv);
            ober.Visitieren(DateTime.Parse("12.12.2012"));
            ober.Visitieren(DateTime.Parse("13.12.2012"));
            Assistenzarzt ass1 = new Assistenzarzt(111, "Isabella", "Mayer", true);
            Assistenzarzt ass2 = new Assistenzarzt(111, "Werner", "Hochmut", false);
            hno.arztHinzufuegen(t1);
            hno.arztHinzufuegen(t2);
            hno.arztHinzufuegen(t3);
            hno.arztHinzufuegen(oberstv);
            hno.arztHinzufuegen(ober);
            hno.arztHinzufuegen(prim);
            hno.arztHinzufuegen(ass1);
            hno.arztHinzufuegen(ass2);


            Assert.AreEqual(expected, hno.ToString());
        }


    }
}
